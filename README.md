# VUnit-CI

## Description
This project demonstrates how to run VUnit tests as a Continuous Integration job on Gitlab CI. As an example, we've implemented a 74161-like counter and a number of unit tests in VHDL.

## License
This project is licensed under the **BSD 3-Clause License**, of which the text in included in this repository in the file `LICENSE` .